
Pod::Spec.new do |s|
  s.name         = "RNCamera"
  s.version      = "1.0.0"
  s.summary      = "RNCamera"
  s.description  = <<-DESC
                  RNCamera
                   DESC
  s.homepage     = ""
  s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "author" => "author@domain.cn" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://github.com/author/RNCamera.git", :tag => "master" }
  s.source_files  = "RNCamera/**/*.{h,m}"
  s.requires_arc = true


  s.dependency "React"
  #s.dependency "others"

end

  
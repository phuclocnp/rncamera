
package com.phucloc.reactnative;

import android.content.Intent;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

public class RNCameraModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;

  public RNCameraModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }

  @Override
  public String getName() {
    return "RNCamera";
  }

  @ReactMethod
  public void openCamera() {
    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    reactContext.startActivity(intent);
  }
}